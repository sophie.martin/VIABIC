#include <math.h>
#include <stdio.h>
#include "dynamics.h"
#include "utils.h"

// 2 Etats : H ci
// 2 controles ai,wi
// 1 tyche sumwj

//Fonctions de calcul de la dynamique de H

float fHnext(float H, float wi, float sumwj){
	float Hnext;
	Hnext = H+R-fminf(H+R,sumwj)-wi;
	return Hnext;
}

//Fonctions de calcul de la dynamique de ci

float GainProduction(float ai,float wi,float Si,float GNCi,float GCi,float Ai,float Bi,float Xi){
	float g = wi/ai/Si;
	if (ai>0) return (1-ai)*Si*GNCi +GCi*ai*Si*(Ai*g-Bi/2*g*g+Xi);
	else return Si*GNCi;
}

float CoutPompage(float wi,float H){
	if ((C0-Z*H)*wi < 0){
		printf("Cout pompage négatif\n");
		getchar();
	}
	return (C0-Z*H)*wi; 
}

float CoutIrrigationAutre(float ai,float wi,float Si){
	return C1*wi+C2/2*wi*wi+C3*wi*ai*Si;
}

float CoutExploitation(float ai,float Si,float DC1i,float DC2i,float DNC1i,float DNC2i){
	return DC1i*ai*Si+ DC2i/2*ai*ai*Si*Si+DNC1i*(1-ai)*Si+ DNC2i/2*(1-ai)*(1-ai)*Si*Si;
}

float Benefices(float H,float ai,float wi,float Si,float GCi,float GNCi,float Ai,float Bi,float Xi,float DC1i,float DC2i,float DNC1i,float DNC2i){
	return  GainProduction(ai,wi,Si,GNCi,GCi,Ai,Bi,Xi) - CoutPompage(wi,H) - CoutIrrigationAutre(ai,wi,Si) - CoutExploitation(ai,Si,DC1i,DC2i,DNC1i,DNC2i);
	//return    - CoutExploitation(ai,Si,DC1i,DC2i,DNC1i,DNC2i);
}
float fcinext(float H, float ci,float ai,float wi,float Si,float GCi,float GNCi,float Ai,float Bi,float Xi,float DC1i,float DC2i,float DNC1i,float DNC2i){
	return ci + GainProduction(ai,wi,Si,GNCi,GCi,Ai,Bi,Xi) - CoutPompage(wi,H) - CoutIrrigationAutre(ai,wi,Si) - CoutExploitation(ai,Si,DC1i,DC2i,DNC1i,DNC2i);
}

//fonction qui calcule une borne supérieure du benefice de farmer_i en faisant comme si les autres de prélevaient pas d'eau

int benefices_maximaux(int farmer_i,float Benef[DH],float Arg_AI[DH],float Arg_WI[DH]){
	float benefcurrent,benef,argai,argwi;
	int i,j,k,AIMAX,AIMIN,WIMAX,WIMIN;
	AIMAX = faimax(farmer_i);
	AIMIN = faimin(farmer_i);
	WIMAX = fwimax(farmer_i);
	WIMIN = fwimin(farmer_i);
	for (k = 0;k<DH;k++){
		benef = -1000000.0;
		argai = -1.0;
		argwi = -1.0;
		for(i=0;i<DAI;i++) {
			for(j=0;j<DWI;j++) {
				if (Valeur(k,HMAX,HMIN,DH)+R>=Valeur(j,WIMAX,WIMIN,DWI)) benefcurrent = Benefices(Valeur(k,HMAX,HMIN,DH),Valeur(i,AIMAX,AIMIN,DAI),Valeur(j,WIMAX,WIMIN,DWI),FARMERS_CHARACTERISTICS[farmer_i][0],FARMERS_CHARACTERISTICS[farmer_i][1],FARMERS_CHARACTERISTICS[farmer_i][2],FARMERS_CHARACTERISTICS[farmer_i][3],FARMERS_CHARACTERISTICS[farmer_i][4],FARMERS_CHARACTERISTICS[farmer_i][5],FARMERS_CHARACTERISTICS[farmer_i][6],FARMERS_CHARACTERISTICS[farmer_i][7],FARMERS_CHARACTERISTICS[farmer_i][8],FARMERS_CHARACTERISTICS[farmer_i][9]);
				//printf("i %d j %d u2 %f %f\n",i,j,Valeur(j,U2MAX,U2MIN,DU2),rcurrent);
				if (benef < benefcurrent) {
					benef = benefcurrent;
					argai = Valeur(i,AIMAX,AIMIN,DAI);
					argwi = Valeur(j,WIMAX,WIMIN,DWI);
				}
			}
		}
		Benef[k]=benef;
		Arg_AI[k]=argai;
		Arg_WI[k]=argwi;

	}
	return 1;
}


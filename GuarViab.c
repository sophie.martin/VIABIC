#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "GuarViab.h"
#include "dynamics.h"
#include "utils.h"
//#include "init.h"

//************************Calcul du noyau de viabilité garanti de l'agriculteur i en fonction des prélèvements maxima des autres joueurs

//L'agriculteur i veut garder un capital ci positif : ci>=0 

// Les négociations entre agriculteurs portent sur des quantités maximales de prélèvement pour chacun wimax<=WIMAX qui est la capacité maximale de de prélèvement de l'agriculteur i
// on pourra également envisager des wimax(H) à puiser dans un ensemble de fonctions dont les paramètres seraient l'objet des négociations

// L'objectif est de calculer les volumes des noyaux de viabilité garantis de chaque agriculteur i puis d'en faire une sorte d'intersection pour en garder la partie utile cf pdf

// Le noyau de viabilité de l'agriculteur i est dans l'espace d'état (ci,H) avec deux controles ai, wi et un tyche sumwj

// La contrainte verifie que ci dans l'ensemble de contraintes (ou autrement dit de satisfaction)*****************************************

// Ensuite fonction qui verifie si un état H, ci est dans l'esnemble de contraintes (ou autrement dit de satisfaction)*****************************************

int IsinConstraintSet(float H,float ci){
	int b=0;
	if (ci>=0) b = 1;
	return b;
}

// Les i engagements sous forme de limitations de l'ensemble des controles admissibles
// fonction qui verifie si la paire de controles (ai,wi) est admissible lorsque l'état du système est (ci,H) étant donné les deux engagements pris *****************************************

int IsinAdmissibleControlSet(float H,float ci,float ai, float wi,float sumwjmax){
	int b=0;
	if (wi<=fmaxf(H+R-sumwjmax,0)) b = 1;
	return b;
}


//Calcul de la frontière inférieure du noyau de viabilité garanti comme tableau de valeurs int IntHfrontiere[DCI] qui associe à des valeurs du capital ci  allant de cimin à cimax l'indice de la valeur minimale du niveau de la nappe H appartenant au noyau de viabilité garanti


void CalculFrontiereNoyau(int farmer_i,int IntHfrontiere[DCI],float engagements_AIMAX[NB_FARMERS],float engagements_AIMIN[NB_FARMERS],float engagements_WIMAX[NB_FARMERS],float engagements_WIMIN[NB_FARMERS]){
	float Hmin,Hmax,cimin,cimax,aimin,aimax,wimin,wimax,sumwjmin,sumwjmax,H,ci,ai,wi,sumwj,r2,r3,m3,Hnext,cinext;

	int change,passage,i,j,b,ii,jj,iii,jjj,intcinext;


//Initialisation des bornes d'exploration
	Hmin = fHmin();
	Hmax= fHmax();
	cimin = fcimin();
	cimax = fcimax();
	aimin = fmaxf(faimin(farmer_i),engagements_AIMIN[farmer_i]);
	aimax = fminf(faimax(farmer_i),engagements_AIMAX[farmer_i]);
	wimin = fmaxf(fwimin(farmer_i),engagements_WIMIN[farmer_i]);
	wimax = fminf(fwimax(farmer_i),engagements_WIMAX[farmer_i]);
	sumwjmin = 0;
	for(j=0;j<NB_FARMERS;j++) {
		if (j!=farmer_i) sumwjmin=sumwjmin+ fmaxf(fwimin(farmer_i),engagements_WIMIN[farmer_i]);
	}

	sumwjmax = 0;
	for(j=0;j<NB_FARMERS;j++) {
		if (j!=farmer_i) sumwjmax=sumwjmax+ fminf(fwimax(farmer_i),engagements_WIMAX[farmer_i]);
	}

// Initialisation de la variable qui indique s'il y aeu un changement
	change=1;
	passage = 1;

//Initialisation des tableaux qui donnent la valeur de la frontière du noyau en tant que H = f(ci) en float et en int (valeur de l'indice correspondant à Hmin étant 0)

	for(i=0;i<DCI;i++) {
		IntHfrontiere[i]=0;
	}

//Calcul
	while(change>0){
		//printf("change %d\n",change);
		change=0;
		//printf("passage %d\n",passage);
		passage = passage + 1;
		for(i=0;i<DCI;i++){
			ci= Valeur(i,cimax,cimin,DCI);
			//printf("i x2  %d %f\n",i,x2);
			for(j=IntHfrontiere[i];j<DH;j++){
				b = 0;
				H = Valeur(j,Hmax,Hmin,DH);
				//printf("j x1 %d %f\n",j,x1);
				if (IsinConstraintSet(H,ci)){
					//printf("IsinConstraintSet\n");
					for(ii=0;ii<DAI;ii++) {
						ai = Valeur(ii,aimax,aimin,DAI);
						for(jj=0;j<DWI;jj++) {
							wi = Valeur(jj,wimax,wimin,DWI);
							//printf("ii u3  %d %f\n",ii,u3);
							if (IsinAdmissibleControlSet(H,ci,ai, wi,sumwjmax)){
								//printf("IsinAdmissibleControlSet\n");
								b = 1;
								for(iii=0;iii<DSUMWJ;iii++) {
									sumwj = Valeur(iii,sumwjmax,sumwjmin,DSUMWJ);
									//printf("iii Rn  %d %f\n",iii,Rn_x3_u1);
									Hnext = fHnext(H,wi,sumwj);
									//printf("x2next %f \n",x2next);
									cinext = fcinext(H,ci,ai,wi,FARMERS_CHARACTERISTICS[farmer_i][0],FARMERS_CHARACTERISTICS[farmer_i][1],FARMERS_CHARACTERISTICS[farmer_i][2],FARMERS_CHARACTERISTICS[farmer_i][3],FARMERS_CHARACTERISTICS[farmer_i][4],FARMERS_CHARACTERISTICS[farmer_i][5],FARMERS_CHARACTERISTICS[farmer_i][6],FARMERS_CHARACTERISTICS[farmer_i][7],FARMERS_CHARACTERISTICS[farmer_i][8],FARMERS_CHARACTERISTICS[farmer_i][9]);

									//printf("x1next %f \n",x1next);
									if (IsinConstraintSet(Hnext,cinext)){
										intcinext = Indice(fminf(cinext,cimax),cimax, cimin,DCI);
										//getchar();
										if (Hnext < Valeur(IntHfrontiere[intcinext],cimax,cimin,DCI) || IntHfrontiere[intcinext] == DH){
											b = 0;
											break;
										}
									}
									else{
										b = 0;
										break;
									}
								}
							}
							if(b == 1){
								//getchar();
								break;
							}
						}
						if(b == 1){
							//getchar();
							break;
						}
					}
				}
				if (b == 1){
					IntHfrontiere[i] = j;
					//printf("intfrontiere %d\n",IntHfrontiere[i]);
					//getchar();
					break;
				}
				else {
					change = change + 1;
					//printf("change %d\n",change);
				}
			}
			if (b == 0){
				IntHfrontiere[i] = DH;
				//printf("intfrontiere %d\n",IntHfrontiere[i]);
				//getchar();
			}
		}

	}
/*	printf("frontiere\n");
	for(i=0;i<DX2;i++){
		printf("%d,",Intx1frontiere[i]);
	}
	printf("\n");
*/
}


//Calcul du pourcentage d'occupation du noyau



float CalculSurfacenoyau(int IntHfrontiere[DCI]){
	float surface;
	int i;
	surface = 0;
	for(i=0;i<DCI;i++) {
		surface=surface+(float)(DH)-(float)(IntHfrontiere[i]);
	}
	surface = surface/(DCI*DH);
	return surface;
}

/*
int O1_CalculTrajectoireViable(float x10,float x20,float x30, int Intx3frontiere[DX3],int Intx1frontiere[DX2],float traj_x1[TMAX],float traj_x2[TMAX],float traj_x3[TMAX],int traj_u1[TMAX],float traj_u2[TMAX],float traj_u3[TMAX]){
	int b=0;
	int t=0;
	int j1,j2,j3,u1,intx1current,intx2current,intx3current;
	float u2,u3,x1current,x2current,x3current,x1,x2,x3,x1next,x2next,x3next, R, E;
	float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12];
	float x1min,x1max,x2min,x2max,x3min,x3max,u2min,u2max,u3min,u3max;

	if (O1_IsinConstraintSetAgricole1D(x30) && O1_IsinConstraintSetREstaurant2D(x10, x20)){
		//Initialisation des bornes d'exploration
		x1min = fx1min();
		x1max= fx1max();
		x2min = fx2min();
		x2max= fx2max();
		u3min = fu3min();
		u3max = fu3max();
		x3min = fx3min();
		x3max= fx3max();
		CreateAssolements(Assolements);
		u2min = fu2min();
		u2max = fu2max();
		x1current = x10;
		x2current = x20;
		x3current = x30;
		while (t<TMAX){
			//printf("xcurrent %f %f %f\n",x1current,x2current,x3current);
			printf(" t %d\n",t);
			b = 0;
			x1 = Valeur(Indice(x1current,x1max, x1min,DX1),x1max, x1min,DX1);
			x2 = Valeur(Indice(x2current,x2max, x2min,DX2),x2max, x2min,DX2);
			x3 = Valeur(Indice(x3current,x3max, x3min,DX3),x3max, x3min,DX3);
			//printf("xrecalé %f %f %f\n",x1,x2,x3);

            for(j2=0;j2<DU2;j2++) {
                u2 = Valeur(j2,u2max,u2min,DU2);
                for(j3=0;j3<DU3;j3++) {
                    u3 = Valeur(j3,u3max,u3min,DU3);
                    for(j1=0;j1<NB_ASSOLEMENTS;j1++) {
                        u1 = j1;
                        R = R_nS(x3,u1,Assolements,u2);
                        E = Cout(u1,Assolements,u2);
                        x1next = O1_fx1next(x1,x2,u3,R,E);
                        x2next = O1_fx2next(x2,u3,R);
                        x3next = O1_fx3next(x3,u1,Assolements,u2,x3min,x3max);
                        //printf("u1 u2 u3 %d %f %f\n",u1,u2,u3);
                        //printf("xnext %f %f %f\n",x1next,x2next,x3next);
                        //if (x2next<1) getchar();
                        if ((x1next > Valeur(Intx1frontiere[Indice(x2next,x2max, x2min,DX2)],x1max,x1min,DX1)) && Intx1frontiere[Indice(x2next,x2max, x2min,DX2)] < DX1 && (Intx3frontiere[Indice(x3next,x3max, x3min,DX3)] == 1)) {
                            b = 1; // viable
                            //getchar();
                            break;
                            }

                        }
                        if (b ==1) break;
                    }
                    if (b ==1) break;

			}
			if (b==0) {printf("problem");getchar();}
			traj_x1[t] = x1;
			traj_x2[t] = x2;
			traj_x3[t] = x3;
			traj_u1[t] = u1;
			traj_u2[t] = u2;
			traj_u3[t] = u3;
			x1current = x1next;
			x2current = x2next;
			x3current = x3next;
			t =t+1;
		}
	}
	else{
        printf("Point de depart non viable.");
	}
	return b;
}

void shuffle_ctrls(int u1[NB_ASSOLEMENTS], int u2[DU2], int u3[DU3]){
    // Mélange une liste de TMAX nombres entiers. 
    int i, j, temp;

    srand(time(NULL));
    for (i = 0; i < NB_ASSOLEMENTS; ++i){
        j = rand() % (NB_ASSOLEMENTS-i) + i;
        temp = u1[i];
        u1[i] = u1[j];
        u1[j] = temp;
}

    for (i = 0; i < DU2; ++i){
            j = rand() % (DU2-i) + i;
            temp = u2[i];
            u2[i] = u2[j];
            u2[j] = temp;
    }
    for (i = 0; i < DU3; ++i){
            j = rand() % (DU3-i) + i;
            temp = u3[i];
            u3[i] = u3[j];
            u3[j] = temp;
    }
}


int O1_CalculTrajectoireViableAlea(float x10,float x20,float x30, int Intx3frontiere[DX3],int Intx1frontiere[DX2],float traj_x1[TMAX],float traj_x2[TMAX],float traj_x3[TMAX],int traj_u1[TMAX],float traj_u2[TMAX],float traj_u3[TMAX]){
    // Calcule une trajectoire aléatoire.
	int b=0;
	int t=0;
	int j1,j2,j3,u1,intx1current,intx2current,intx3current;
	float u2,u3,x1current,x2current,x3current,x1,x2,x3,x1next,x2next,x3next, R, E;
	float Assolements[NB_ASSOLEMENTS][6*NB_CROPS+12];
	float x1min,x1max,x2min,x2max,x3min,x3max,u2min,u2max,u3min,u3max;
	int indu1[NB_ASSOLEMENTS], indu2[DU2], indu3[DU3];

	if (O1_IsinConstraintSetAgricole1D(x30) && O1_IsinConstraintSetREstaurant2D(x10, x20)){
		//Initialisation des bornes d'exploration
		x1min = fx1min();
		x1max= fx1max();
		x2min = fx2min();
		x2max= fx2max();
		u3min = fu3min();
		u3max = fu3max();
		x3min = fx3min();
		x3max= fx3max();
		CreateAssolements(Assolements);
		u2min = fu2min();
		u2max = fu2max();
		x1current = x10;
		x2current = x20;
		x3current = x30;

        for (j1=0; j1<NB_ASSOLEMENTS; j1++){indu1[j1] = j1;}
		for (j2=0; j2<DU2; j2++){indu2[j2] = j2;}
		for (j3=0; j3<DU3; j3++){indu3[j3] = j3;}

		while (t<TMAX){
			printf("xcurrent %f %f %f\n",x1current,x2current,x3current);
			printf(" t %d\n",t);
			b = 0;
			x1 = Valeur(Indice(x1current,x1max, x1min,DX1),x1max, x1min,DX1);
			x2 = Valeur(Indice(x2current,x2max, x2min,DX2),x2max, x2min,DX2);
			x3 = Valeur(Indice(x3current,x3max, x3min,DX3),x3max, x3min,DX3);
			//printf("xrecalé %f %f %f\n",x1,x2,x3);

			shuffle_ctrls(indu1, indu2, indu3);

            for(j3=0;j3<DU3;j3++) {
                u3 = Valeur(indu3[j3],u3max,u3min,DU3);
                for(j2=0;j2<DU2;j2++) {
                    u2 = Valeur(indu2[j2],u2max,u2min,DU2);
                    for(j1=0;j1<NB_ASSOLEMENTS;j1++) {
                        u1 = indu1[j1];
                        R = R_nS(x3,u1,Assolements,u2);
                        E = Cout(u1,Assolements,u2);
                        x1next = O1_fx1next(x1,x2,u3,R,E);
                        x2next = O1_fx2next(x2,u3,R);
                        x3next = O1_fx3next(x3,u1,Assolements,u2,x3min,x3max);
                        //printf("u1 u2 u3 %d %f %f\n",u1,u2,u3);
                        //printf("xnext %f %f %f\n",x1next,x2next,x3next);
                        //if (x2next<1) getchar();
                        if ((x1next > Valeur(Intx1frontiere[Indice(x2next,x2max, x2min,DX2)],x1max,x1min,DX1)) && Intx1frontiere[Indice(x2next,x2max, x2min,DX2)] < DX1 && (Intx3frontiere[Indice(x3next,x3max, x3min,DX3)] == 1)) {
                            b = 1; // viable
                            //getchar();
                            break;
                            }

                        }
                        if (b ==1) break;
                    }
                    if (b ==1) break;

			}
			if (b==0) {printf("problem");getchar();}
			traj_x1[t] = x1;
			traj_x2[t] = x2;
			traj_x3[t] = x3;
			traj_u1[t] = u1;
			traj_u2[t] = u2;
			traj_u3[t] = u3;
			x1current = x1next;
			x2current = x2next;
			x3current = x3next;
			t =t+1;
		}
	}
	else{
        printf("Point de depart non viable.");
	}
	return b;
}*/

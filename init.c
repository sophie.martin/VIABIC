#include "init.h"
//Chaqe exploitation a comme caractéristiques float Si,float GCi,float GNCi,float Ai,float Bi,float Xi,float DC1i,float DC2i,float DNC1i,float DNC2i,float AIMAX,float AIMIN,float WIMAX,float WMIN
// soit 10 éléments
//Blé tendre Katrin
const float FARMERS_CHARACTERISTICS[NB_FARMERS][14] = {2.0, 50.0, 0.0, 0.0051176, 0.00000214, 7.144896,490.0,0.5,200.0,0.5,1.0,0.0,30.0,0.0,//
2.0, 350.0, 50.0, 0.0051176, 0.00000214, 7.144896,490.0,0.5,0.0,0.0,1.0,0.0,30.0,0.0};


//*************************D abord les bornes de toutes les variables*********servira peut etre plus tard si on a envie de les changer ....
//4 Fonctions qui "calculent" les bornes du domaine considéré pour les valeurs des 2 états : H et ci

float fHmax(){
	float hmax;
	hmax =  HMAX;
	return hmax;
}

float fHmin(){
	float hmin;
	hmin = HMIN;
	return hmin;
}

float fcimax(){
	float cimax;
	cimax =  CIMAX;
	return cimax;
}

float fcimin(){
	float cimin;
	cimin =  CIMIN;
	return cimin;
}


//4 Fonctions qui "calculent" les bornes du domaine considéré pour les valeurs des 2 controles : ai et wi

float faimax(int farmer_i){
	float aimax;
	aimax =  FARMERS_CHARACTERISTICS[farmer_i][10];
	return aimax;
}

float faimin(int farmer_i){
	float aimin;
	aimin =  FARMERS_CHARACTERISTICS[farmer_i][11];
	return aimin;
}

float fwimax(int farmer_i){
	float wimax;
	wimax =  FARMERS_CHARACTERISTICS[farmer_i][12];
	return wimax;
}

float fwimin(int farmer_i){
	float wimin;
	wimin =  FARMERS_CHARACTERISTICS[farmer_i][13];
	return wimin;
}


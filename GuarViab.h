#ifndef _GUARVIAB_H_
#define _GUARVIAB_H_

#include "init.h"
#include "dynamics.h"

//Pour l'évaluation de l'organisation 1 en fonction des différentes valeurs des engagements


//************************Calcul du noyau de viabilité garanti de l'agriculteur i en fonction des prélèvements maxima des autres joueurs

//L'agriculteur i veut garder un capital ci positif : ci>=0 

// Les négociations entre agriculteurs portent sur des quantités maximales de prélèvement pour chacun wimax<=WIMAX qui est la capacité maximale de de prélèvement de l'agriculteur i
// on pourra également envisager des wimax(H) à puiser dans un ensemble de fonctions dont les paramètres seraient l'objet des négociations

// L'objectif est de calculer les volumes des noyaux de viabilité garantis de chaque agriculteur i puis d'en faire une sorte d'intersection pour en garder la partie utile cf pdf

// Le noyau de viabilité de l'agriculteur i est dans l'espace d'état (ci,H) avec deux controles ai, wi et un tyche sumwj

// La contrainte verifie que ci dans l'ensemble de contraintes (ou autrement dit de satisfaction)*****************************************

// Ensuite fonction qui verifie si un état H, ci est dans l'esnemble de contraintes (ou autrement dit de satisfaction)*****************************************

int IsinConstraintSet(float H,float ci);

// Les i engagements sous forme de limitations de l'ensemble des controles admissibles
// fonction qui verifie si la paire de controles (ai,wi) est admissible lorsque l'état du système est (ci,H) étant donné les deux engagements pris *****************************************

int IsinAdmissibleControlSet(float H,float ci,float ai, float wi,float sumwjmax);


//Calcul de la frontière inférieure du noyau de viabilité garanti comme tableau de valeurs int IntHfrontiere[DCI] qui associe à des valeurs du capital ci  allant de cimin à cimax l'indice de la valeur minimale du niveau de la nappe H appartenant au noyau de viabilité garanti


void CalculFrontiereNoyau(int farmer_i,int IntHfrontiere[DCI],float engagements_AIMAX[NB_FARMERS],float engagements_AIMIN[NB_FARMERS],float engagements_WIMAX[NB_FARMERS],float engagements_WIMIN[NB_FARMERS]);


//Calcul du pourcentage d'occupation du noyau



float CalculSurfacenoyau(int IntHfrontiere[DCI]);

#endif

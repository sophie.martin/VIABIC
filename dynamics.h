#ifndef _dynamics_H_
#define _dynamics_H_

#include "init.h"

//Paramètres des dynamiques

// Pour H

#define R 20.0 //remplissage annuel de la nappe

//Pour ci
#define C0 0.091 //cf Katrin
#define Z 0.00045 //cf Katrin à faire
#define C1 0.052 // cf Katrin
#define C2 0.0 // cf Katrin à modifier eventuellement
#define C3 0.0 // cf Katrin à modifier eventuellement



// Paramètres pour les contraintes
#define X3QUAL 0.2
#define X1V 0.0


// Pour les trajectoires
#define TMAX 20


//Fonctions de calcul de la dynamique de H
float fHnext(float H, float wi, float sumwj);

//Fonctions de calcul de la dynamique de ci

float GainProduction(float ai,float wi,float Si,float GNCi,float GCi,float Ai,float Bi,float Xi);

float CoutPompage(float wi,float H);

float CoutIrrigationAutre(float ai,float wi,float Si);

float CoutExploitation(float ai,float Si,float DC1i,float DC2i,float DNC1i,float DNC2i);

float Benefices(float H,float ai,float wi,float Si,float GCi,float GNCi,float Ai,float Bi,float Xi,float DC1i,float DC2i,float DNC1i,float DNC2i);

float fcinext(float H, float ci,float ai,float wi,float Si,float GCi,float GNCi,float Ai,float Bi,float Xi,float DC1i,float DC2i,float DNC1i,float DNC2i);

//fonction qui calcule une borne supérieure du benefice

int benefices_maximaux(int farmer_i,float Benef[DH],float Arg_AI[DH],float Arg_WI[DH]);

#endif


#ifndef _AFFICHAGE_H_
#define _AFFICHAGE_H_

#include "GuarViab.h"

#define PREFIXEFICHIER "GuarViab_VIABIC"
#define VERSION "0.0"

void InscriptionParametres(FILE *f,float engagements_AIMAX[NB_FARMERS],float engagements_AIMIN[NB_FARMERS],float engagements_WIMAX[NB_FARMERS],float engagements_WIMIN[NB_FARMERS]);

void Affichage_Frontiere_Noyau_2D_ciH(FILE *f,int Values[DCI]);



#endif

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include "GuarViab.h"
#include "utils.h"
#include "dynamics.h"
#include "affichage.h"
#include "init.h"



//Calcul des valeurs max possibles des benefices en fonction de H avec les arg max des controles et les details pour verification
int main(void){
	float Benef[DH],Arg_AI[DH],Arg_WI[DH];
	int k,b;
	int farmer_i=0;
	b = benefices_maximaux(farmer_i,Benef,Arg_AI,Arg_WI);
	for (k = 0;k<DH;k++){
		printf("%d %f %f %f / %f %f %f %f\n",k,Benef[k],Arg_AI[k],Arg_WI[k],GainProduction(Arg_AI[k],Arg_WI[k],FARMERS_CHARACTERISTICS[farmer_i][0],FARMERS_CHARACTERISTICS[farmer_i][2],FARMERS_CHARACTERISTICS[farmer_i][1],FARMERS_CHARACTERISTICS[farmer_i][3],FARMERS_CHARACTERISTICS[farmer_i][4],FARMERS_CHARACTERISTICS[farmer_i][5]), CoutPompage(Arg_WI[k],Valeur(k,HMAX,HMIN,DH)) , CoutIrrigationAutre(Arg_AI[k],Arg_WI[k],FARMERS_CHARACTERISTICS[farmer_i][0]) , CoutExploitation(Arg_AI[k],FARMERS_CHARACTERISTICS[farmer_i][0],FARMERS_CHARACTERISTICS[farmer_i][6],FARMERS_CHARACTERISTICS[farmer_i][7],FARMERS_CHARACTERISTICS[farmer_i][8],FARMERS_CHARACTERISTICS[farmer_i][9]));
	}	
	return 1;
}


//Pour calculer les noyaux de tous les agriculteurs mais AVANT faut se METTRE D ACCORD sur les dynamiques 
/*
int main(int argc, char *argv[]){
	int b,i,j;
	int IntHfrontiere[DCI];
	float surface;
	float engagements_AIMAX[NB_FARMERS]={1,1};
	float engagements_AIMIN[NB_FARMERS]={0,0};
	float engagements_WIMAX[NB_FARMERS]={20,100};
	float engagements_WIMIN[NB_FARMERS]={0,0};
	FILE *f;
	f =fopen(PREFIXEFICHIER,"w");
	InscriptionParametres(f,engagements_AIMAX,engagements_AIMIN,engagements_WIMAX,engagements_WIMIN);
	fprintf(f,"Si chacun respecte ses engagements, voici le noyau de chacun des %d agriculteurs :\n",NB_FARMERS);
	for (i=0;i<NB_FARMERS;i++){
		fprintf(f,"Agriculteur %d :\n",i);
		CalculFrontiereNoyau(i,IntHfrontiere,engagements_AIMAX,engagements_AIMIN,engagements_WIMAX,engagements_WIMIN);
		Affichage_Frontiere_Noyau_2D_ciH(f,IntHfrontiere);
		surface = CalculSurfacenoyau(IntHfrontiere);
		fprintf(f,"Surface du noyau %f\n",surface);
	}
	fclose(f);
	return 0;
}*/


#include <stdio.h>
#include "affichage.h"
#include "init.h"
#include "utils.h"
#include "dynamics.h"
#include "GuarViab.h"
#include "affichage.h"

//Inscription dans un fichier
//Valeurs des paramètres

//Inscription préambule information sur les paramètres généraux
void InscriptionParametres(FILE *f,float engagements_AIMAX[NB_FARMERS],float engagements_AIMIN[NB_FARMERS],float engagements_WIMAX[NB_FARMERS],float engagements_WIMIN[NB_FARMERS]){
	int j,k;
	fprintf(f,"Parametres\n");
        fprintf(f,"VERSION : %s\n",VERSION);
        fprintf(f,"Parametres de la dynamique\n");
        fprintf(f,"R : %f\n",R);
        fprintf(f,"C0 : %f\n",C0);
        fprintf(f,"Z : %f\n",Z);
        fprintf(f,"C1 : %f\n",C1);
        fprintf(f,"C2 : %f\n",C3);
        fprintf(f,"\n");
	fprintf(f,"Parametres des agriculteurs\n");
	fprintf(f,"Nombre d'agriculteurs %d\n",NB_FARMERS);
	fprintf(f,"Parametres des %d agriculteurs dans l'ordre float Si,float GCi,float GNCi,float Ai,float Bi,float Xi,float DC1i,float DC2i,float DNC1i,float DNC2i,float AIMAX,float AIMIN,float WIMAX,float WMIN\n",NB_FARMERS);
	for (k=0;k<NB_FARMERS;k++){
		for (j=0;j<14;j++){
			fprintf(f,"%f ", FARMERS_CHARACTERISTICS[k][j]);
		}
		fprintf(f,"\n");
	}
	fprintf(f,"En outre les agriculteurs ont pris les engagements suivants\n Concernant AIMAX : \n");
	for (k=0;k<NB_FARMERS;k++){
		fprintf(f,"%f ", engagements_AIMAX[k]);
	}
	fprintf(f,"\n Concernant AIMIN:\n");
	for (k=0;k<NB_FARMERS;k++){
		fprintf(f,"%f ", engagements_AIMIN[k]);
	}
	fprintf(f,"\n Concernant WIMAX:\n");
	for (k=0;k<NB_FARMERS;k++){
		fprintf(f,"%f ", engagements_WIMAX[k]);
	}
	fprintf(f,"\n Concernant WIMIN:\n");
	for (k=0;k<NB_FARMERS;k++){
		fprintf(f,"%f ", engagements_WIMIN[k]);
	}
	
	fprintf(f,"\n");
	fprintf(f,"Parametres de la grille et de la discretisation\n");
	fprintf(f,"HMAX %f\n",HMAX);
	fprintf(f,"HMIN %f\n",HMIN);
	fprintf(f,"CIMAX %f\n",CIMAX);
	fprintf(f,"CIMIN %f\n",CIMIN);
/*	fprintf(f,"WIMAX %f\n",HMAX);
	fprintf(f,"WIMIN %f\n",HMIN);
	fprintf(f,"AIMAX %f\n",AIMAX);
	fprintf(f,"AIMIN %f\n",AIMIN);*/
	fprintf(f,"DH %d\n",DH);
	fprintf(f,"DCI %d\n",DCI);
	fprintf(f,"DWI %d\n",DWI);
	fprintf(f,"DAI %d\n",DAI);
	fprintf(f,"DSUMWJ %d\n",DSUMWJ);
	fprintf(f,"\n");
}


/*
void O1_InscriptionResultats(FILE *f, float surfaces_Agricole1D[O1_DR][O1_DE], float surfaces_Restaurant2D[O1_DR][O1_DE]){
    float Rmin, Emax;
    int i, j;

    float RMAX = production_maximale();
	float EMAX = cout_maximal();

    for (i=0;i<O1_DR;i++){
		if (i==0) fprintf(f,"[");
		else fprintf(f,",");
		Rmin = Valeur(i,RMAX,0.0,O1_DR);
		fprintf(f,"%f",Rmin);
		if (i==O1_DR-1) fprintf(f,"]\n");
	}
	fprintf(f,"\n");
	for (i=0;i<O1_DE;i++){
		if (i==0) fprintf(f,"[");
		else fprintf(f,",");
		Emax = Valeur(i,EMAX,0.0,O1_DE);
		fprintf(f,"%f",Emax);
		if (i==O1_DE-1) fprintf(f,"]\n");
	}
	fprintf(f,"\n RESTAURANT\n");
	for (i=0;i<O1_DE;i++){
		if (i==0) fprintf(f,"[");
		else fprintf(f,",");
		for (j=0;j<O1_DR;j++){
			if (j==0) fprintf(f,"[");
			else fprintf(f,",");
			fprintf(f,"%f",surfaces_Restaurant2D[j][i]);
			if (j==O1_DR-1) fprintf(f,"]");
		}
		if (i==O1_DE-1) fprintf(f,"]\n");
	}

	fprintf(f,"\n AGRICULTURE\n");
	for (i=0;i<O1_DE;i++){
		if (i==0) fprintf(f,"[");
		else fprintf(f,",");
		for (j=0;j<O1_DR;j++){
			if (j==0) fprintf(f,"[");
			else fprintf(f,",");
			fprintf(f,"%f",surfaces_Agricole1D[j][i]);
			if (j==O1_DR-1) fprintf(f,"]");
		}
		if (i==O1_DE-1) fprintf(f,"]\n");
    }
    fprintf(f,"\n");
    fprintf(f,"\n");
    fprintf(f,"\n");
}
*/




void Affichage_Frontiere_Noyau_2D_ciH(FILE *f,int Values[DCI]){
	int i;
	fprintf(f,"Valeurs de la grille\n En abscisse ci :\n ci = [");
	for (i=0;i<DCI;i++){
		fprintf(f,"%f",Valeur(i,CIMAX,CIMIN,DCI));
		if (i < DCI-1) fprintf(f,",");
	}
	fprintf(f,"]\n En ordonnees H :\n H = [");
	for (i=0;i<DH;i++){
		fprintf(f,"%f",Valeur(i,HMAX,HMIN,DH));
		if (i < DH-1) fprintf(f,",");
	}
	fprintf(f,"]\n H en fonction de ci sous forme d'entier representant le numero du point de la grille :\n");
	fprintf(f,"Noyau = [");
	for (i=0;i<DCI;i++){
		fprintf(f,"%d",Values[i]);
		if (i < DCI-1) fprintf(f,",");
	}
	fprintf(f,"]\n");
}

/*
void Affichage_Traj(FILE *f, float traj_x1[TMAX],float traj_x2[TMAX],float traj_x3[TMAX],int traj_u1[TMAX],float traj_u2[TMAX],float traj_u3[TMAX]){
    int i;

    fprintf(f, "T = %d\n", TMAX);

       fprintf(f,"traj_x1 = [");
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_x1[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_x2 = [");
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_x2[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_x3 = [");
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_x3[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_u1 = [");
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%d",traj_u1[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_u2 = [");
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_u2[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_u3 = [");
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_u3[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
}

void Affichage_Traj_i(FILE *f, float traj_x1[TMAX],float traj_x2[TMAX],float traj_x3[TMAX],int traj_u1[TMAX],float traj_u2[TMAX],float traj_u3[TMAX],int n){
    int i;

       fprintf(f,"traj_x1_%d = [",n);
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_x1[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_x2_%d = [",n);
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_x2[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_x3_%d = [",n);
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_x3[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_u1_%d = [",n);
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%d",traj_u1[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_u2_%d = [",n);
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_u2[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
    fprintf(f,"traj_u3_%d = [",n);
    for (i=0;i<TMAX;i++){
    	fprintf(f,"%f",traj_u3[i]);
    	if (i<TMAX-1) fprintf(f,",");
    }
    fprintf(f,"]\n");
}
*/
//Sortie destinée à être copiée_collée dans le Notebook python
/*
void Affichage_pour_affichage_python(FILE *f,int Values_O1_x3[DX3],int Values_O1_x2x1[DX2],int ValuesU[DX2][DX3]){
	int i,j,k;
	fprintf(f,"# A récuperer du .c\n## concernant les grilles : Grille_x1 tableau de taille DX1; Grille_x2 tableau de taille DX2 ; Grille_x3 tableau de taille DX3\n");
	fprintf(f,"Grille_x1 = ");
	Affichage_vecteur(f,X1MAX,X1MIN,DX1);
	fprintf(f,"Grille_x2 = ");
	Affichage_vecteur(f,X2MAX,X2MIN,DX2);
	fprintf(f,"Grille_x3 = ");
	Affichage_vecteur(f,X3MAX,X3MIN,DX3);
	fprintf(f,"#Concernant les noyaux\n#Le noyau O1 Agricole1D\n");
	fprintf(f,"O1_Noyau_x3 = ");
	Affichage_Noyau_1D_x3(f,Values_O1_x3);
	fprintf(f,"#Le noyau O1 Restaurant 2D\n");
	fprintf(f,"O1_Frontiere_Noyau_x2x1 =");
	Affichage_Frontiere_Noyau_2D_x2x1(f,Values_O1_x2x1);
	fprintf(f,"#Le noyau Organisation Unique 3D\n");
	fprintf(f,"U_Frontiere_Noyau_x2x3x1 =");
	Affichage_Frontiere_Noyau_3D_x2x3x1(f,ValuesU);
}



void SaveKernel(FILE *f,int ValuesU[DX2][DX3]){
   fwrite(ValuesU, sizeof(int), DX2*DX3, f);
}


void LoadKernel(FILE *f,int ValuesU[DX2][DX3]){
    fread(ValuesU, sizeof(int), DX2*DX3, f);
}*/

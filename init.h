#ifndef merdeINIT_H_
#define merdeINIT_H_

//Paramètres population de fermiers
#define NB_FARMERS 2
//Chaqe exploitation a comme caractéristiques float Si,float GCi,float GNCi,float Ai,float Bi,float Xi,float DC1i,float DC2i,float DNC1i,float DNC2i,float AIMAX,float AIMIN,float WIMAX,float WMIN
// soit 10 éléments
//Blé tendre Katrin
const float FARMERS_CHARACTERISTICS[NB_FARMERS][14];


//Paramètres boites
#define HMAX 200.0
#define HMIN 0.0

#define CIMAX 20.0
#define CIMIN 0.0
/*#define WIMAX 300.0
#define WIMIN 0.0
#define AIMAX 1.0
#define AIMIN 0.0*/

#define SUMWJMAX 1.0

//Pour les grilles
#define DWI 21 //11 // 301
#define DAI 21 //11 // 301
#define DH 51 // 301
#define DCI 101 //1501 //1001 // 1001 //541//441
#define DSUMWJ 21 //lié à la discrétisation des valeurs des autres prelevements




//*************************D abord les bornes de toutes les variables*********servira peut etre plus tard si on a envie de les changer ....

//4 Fonctions qui "calculent" les bornes du domaine considéré pour les valeurs des 2 états : H et ci

float fHmax();

float fHmin();

float fcimax();

float fcimin();


//4 Fonctions qui "calculent" les bornes du domaine considéré pour les valeurs des 2 controles : ai et wi

float faimax(int farmer_i);	

float faimin(int farmer_i);

float fwimax(int farmer_i);

float fwimin(int farmer_i);

#endif
